function attachScriptTag(apiKey) {
  var script = document.createElement('script');
  script.type = 'text/javascript';

  const base = 'https://maps.googleapis.com/maps/api/js';
  const callback = 'googleMapsApiLoaderCallback';
  script.src = `${base}?key=${apiKey}&callback=${callback}`;

  document.querySelector('head').appendChild(script);
}

export default async function() {
  const apiKey = document.querySelector('body').dataset.gmapsApiKey;

  // Google Maps API only supports callbacks passed-in via the url
  const googleMapsApi = new Promise(resolve => {
    window.googleMapsApiLoaderCallback = function() {
      resolve(window.google);
    }
  });

  attachScriptTag(apiKey);
  return googleMapsApi;
}
