import googleApiLoader from './google-api-loader.js';

(async function main() {

  const google = await googleApiLoader();

  // adapted from: https://goo.gl/4QZ5Wo
  const el = document.getElementById('map');
  const uluru = {lat: -25.363, lng: 131.044};
  const map = new google.maps.Map(el, { center: uluru, zoom: 4 });
  const marker = new google.maps.Marker({ position: uluru, map });

})();
