# Google Maps Fun

TIL google maps does not have a nice async solution.


## Getting started

1. Checkout this repo

1. Open `index.html` using a web server.

    Why is a server needed? Because js modules and the import will blow up when CORS are not available with `file://`

   Simple servers:
   ```bash
   # python 3
   python -m http.server
   # python 2
   python -m SimpleHTTPServer
   # node
   npm install http-server
   $(npm bin)/http-server
   ```

## Todo Ideas

Demo a marker generator that is async & throttled.
